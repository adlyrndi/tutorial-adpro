package id.ac.ui.cs.advprog.tutorialadpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialAdproApplication {

    public static void main(String[] args) {
        SpringApplication.run(TutorialAdproApplication.class, args);
    }

}
