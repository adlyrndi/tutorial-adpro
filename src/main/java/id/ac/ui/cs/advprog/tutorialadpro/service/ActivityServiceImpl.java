package id.ac.ui.cs.advprog.tutorialadpro.service;
import id.ac.ui.cs.advprog.tutorialadpro.model.Activity;
import id.ac.ui.cs.advprog.tutorialadpro.model.Day;
import id.ac.ui.cs.advprog.tutorialadpro.repository.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    private ActivityRepository activityRepository;

    @Override
    public Activity create(Activity activity) {
        activityRepository.create(activity);
        return activity;
    }

    @Override
    public List<Activity> findAll() {
        Iterator<Activity> activityIterator = activityRepository.findAll();
        List<Activity> allActivity = new ArrayList<>();
        activityIterator.forEachRemaining(allActivity::add);
        return allActivity;
    }

    @Override
    public List<Activity> findByDay(Day day) {
        // TO DO: get a list of activities that match the day
        Iterator<Activity> iterator = activityRepository.findAll();
        List<Activity> allActivity = new ArrayList<>();

        iterator.forEachRemaining(allActivity::add);

        List<Activity> filter = getFilteredList(allActivity, day);
        return filter;

    }

    public List<Activity> getFilteredList(List<Activity> fooList, Day day) {
        List<Activity> nameList = new ArrayList<Activity>();
        for(Activity foo : fooList) {
            if(day.equals(foo.getDay())) {
                nameList.add(foo);
            }
        }
        System.out.println(nameList);
        return nameList;
    }

}

